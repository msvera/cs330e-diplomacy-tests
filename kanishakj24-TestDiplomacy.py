#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 17:04:27 2020

@author: varunmeduri
"""

# TestDiplomacy.py
from io import StringIO

from unittest import main,TestCase
from Diplomacy import initial_position_array_creator, process_moves , solver

class TestDiplomacy(TestCase):

    def test_solve0(self):
        reader = StringIO("A SanAntonio Hold\nB SantaFe Move SanAntonio\nC Delhi Support A\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A SanAntonio\nB [dead]\nC Delhi\n")

    def test_solve1(self):
        reader = StringIO("A SanAntonio Hold\nB SantaFe Move SanAntonio\nC Delhi Support A\nD Beijing Support B\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A [dead]\nB [dead]\nC Delhi\nD Beijing\n")

    def test_solve2(self):
        reader = StringIO("A SanAntonio Move SantaFe\nB SantaFe Hold\nC Delhi Move SanAntonio\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A [dead]\nB [dead]\nC SanAntonio\n")
    
    def test_solve3(self):
        reader = StringIO("A SanAntonio Support B\nB SantaFe Move Delhi\nC Delhi Hold\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A SanAntonio\nB Delhi\nC [dead]\n")
    
    def test_solve4(self):
        reader = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve5(self):
        reader = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        writer = StringIO()
        solver(reader,writer)
        self.assertEqual(writer.getvalue(),"A [dead]\nB Madrid\nC London\n")

if __name__ == "__main__" : #pragma: no cover
    main()
