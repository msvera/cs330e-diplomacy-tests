#!/usr/bin/env python3
# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # diplomacy_solve
    # ----
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    def test_solve2(self):
        r = StringIO("B NewYork Move Houston\nA Houston Hold\nC LA Support B\nD Austin Support A\nE Chicago Move Austin\nF DC Support A\nG London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Houston\nC LA\nD [dead]\nE [dead]\nF DC\nG London\n")
    def test_solve_3(self):
        r = StringIO("ArmyA CityA Hold\nArmyB CityB Support ArmyA\nArmyC CityC Move CityB")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "ArmyA CityA\nArmyB [dead]\nArmyC [dead]\n")
    
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
