from unittest import main, TestCase

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from Diplomacy import *


class TestDiplomacy(TestCase):

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)

        for k in test.armies:
            self.assertEqual(test.armies[k].name, "A")
            self.assertEqual(test.armies[k].city, "Madrid")
            self.assertEqual(test.armies[k].action, "Hold")
            self.assertEqual(test.armies[k].target, None)

        # checking for inputs with 3 arguments

    def test_read_2(self):
        s = StringIO("A Madrid Move London\nB Paris Support A\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        self.assertEqual(test.armies["A"].name, "A")
        self.assertEqual(test.armies["A"].city, "Madrid")
        self.assertEqual(test.armies["A"].action, "Move")
        self.assertEqual(test.armies["A"].target, "London")
        self.assertEqual(test.armies["B"].name, "B")
        self.assertEqual(test.armies["B"].city, "Paris")
        self.assertEqual(test.armies["B"].action, "Support")
        self.assertEqual(test.armies["B"].target, "A")
        self.assertEqual(len(test.armies), 2)

        # checking for multi-line inputs with 4 arguments (move -> destination)

    def test_read_3(self):
        s = StringIO("\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        self.assertEqual(test.armies, {})
        # checking for empty input

    def test_read_4(self):
        s = StringIO("C London Support B\n")
        test = Diplomacy()
        test.diplomacy_read(s)

        for k in test.armies:
            self.assertEqual(test.armies[k].name, "C")
            self.assertEqual(test.armies[k].city, "London")
            self.assertEqual(test.armies[k].action, "Support")
            self.assertEqual(test.armies[k].target, "B")

        # checking for inputs with 4 arguments (support -> name)

    def test_move_1(self):
        s = StringIO("A Madrid Move London\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        test.diplomacy_move()

        for k in test.armies:
            self.assertEqual(test.armies[k].name, "A")
            self.assertEqual(test.armies[k].city, "London")

        # default test for "Move" and "Hold"

    def test_move_2(self):
        s = StringIO("A Madrid Move London\nB Barcelona Move Madrid\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        test.diplomacy_move()
        self.assertEqual(test.armies["A"].name, "A")
        self.assertEqual(test.armies["A"].city, "London")
        self.assertEqual(test.armies["B"].name, "B")
        self.assertEqual(test.armies["B"].city, "Madrid")
        self.assertEqual(len(test.armies), 2)
        # test if can move multiple armies

    def test_move_3(self):
        s = StringIO("A Madrid Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        test.diplomacy_move()

        for k in test.armies:
            self.assertEqual(test.armies[k].name, "A")
            self.assertEqual(test.armies[k].city, "Madrid")

        # test if "Hold" works

    def test_group_1(self):
        s = StringIO("A Madrid Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        g = test.diplomacy_group()

        for k, v in g.items():
            self.assertEqual(k, "Madrid")
            self.assertEqual(v[0].name, "A")
            self.assertEqual(v[0].city, "Madrid")
            self.assertEqual(v[0].action, "Hold")

        # default test to see if input reads into dictionary keys and values

    def test_group_2(self):
        s = StringIO("A Madrid Hold\n B Madrid Hold\n C London Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        g = test.diplomacy_group()
        self.assertEqual(sorted(list(g.keys())), ["London", "Madrid"])
        self.assertEqual(len(g["Madrid"]), 2)
        self.assertEqual(len(g["London"]), 1)
        # test if multiple armies can be grouped into 1 city

    def test_support_1(self):
        s = StringIO("A Madrid Hold\n B Madrid Hold\n C London Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        g = test.diplomacy_group()
        test.diplomacy_support(g)
        self.assertEqual(test.armies["A"].supported, 0)
        self.assertEqual(test.armies["B"].supported, 0)
        self.assertEqual(test.armies["C"].supported, 0)
        # testing scenario with no supports and no conflicts

    def test_support_2(self):
        s = StringIO("A Madrid Hold\n B Paris Support A\n C London Hold\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        test.diplomacy_move()
        g = test.diplomacy_group()
        test.diplomacy_support(g)
        self.assertEqual(test.armies["A"].supported, 1)
        self.assertEqual(test.armies["B"].supported, 0)
        self.assertEqual(test.armies["C"].supported, 0)
        # testing scenario with supports and no conflicts

    def test_support_3(self):
        s = StringIO(
            "A Madrid Hold\n B Paris Support A\n C London Move Paris\n")
        test = Diplomacy()
        test.diplomacy_read(s)
        test.diplomacy_move()
        g = test.diplomacy_group()
        test.diplomacy_support(g)
        self.assertEqual(test.armies["A"].supported, 0)
        self.assertEqual(test.armies["B"].supported, 0)
        self.assertEqual(test.armies["C"].supported, 0)
        # testing scenario with supports but conflicts that cancel them out

    def test_solve_1(self):
        test = Diplomacy()
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        test.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        # base case scenario

    def test_solve_2(self):
        test = Diplomacy()
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        test.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
        # scenario where all armies die

    def test_solve_3(self):
        test = Diplomacy()
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid")
        w = StringIO()
        test.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Madrid\n")
        # scenario where no armies die

    def test_solve_4(self):
        test = Diplomacy()
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Hold\nC London Move Barcelona\nD Austin Support A\nE Paris Support B")
        w = StringIO()
        test.diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Austin\nE Paris\n")
        # scenario where multiple armies have supports

    def test_solve_5(self):
        test = Diplomacy()
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Hold\nC London Move Barcelona\nD Austin Support A\nE Paris Support B\nF Houston Support A")
        w = StringIO()
        test.diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB [dead]\nC [dead]\nD Austin\nE Paris\nF Houston\n")
        # scenario where some armies have multiple supports


def main():  # pragma: no cover
    test = TestDiplomacy()
    test.test_read_1()
    test.test_read_2()
    test.test_read_3()
    test.test_read_4()
    test.test_move_1()
    test.test_move_2()
    test.test_move_3()
    test.test_group_1()
    test.test_group_2()
    test.test_support_1()
    test.test_support_2()
    test.test_support_3()
    test.test_solve_1()
    test.test_solve_2()
    test.test_solve_3()
    test.test_solve_4()
    test.test_solve_5()


if __name__ == "__main__":  # pragma no cover
    main()
